class VictoriaConfig {
  static getURL() {
    return process.env.VM_SELECT_LINK
      ? `http://${process.env.VM_SELECT_LINK}/select/1/prometheus/api/v1/`
      : 'http://dev-user:PVwKrgvBoG5rcdAM2DoTBPdQk05ihzTCXwAY1ff1HlmsIBWS@vm-dev.linked-green.com/select/1/prometheus/api/v1/';
  }

  static getConfig(): any {
    return {
      user: process.env.VM_USERNAME || 'dev-user',
      password: process.env.VM_PASSWORD || 'PVwKrgvBoG5rcdAM2DoTBPdQk05ihzTCXwAY1ff1HlmsIBWS',
      link: process.env.VM_INSERT_LINK || 'vm-dev.linked-green.com',
    };
  }

  static postURL() {
    return process.env.VM_INSERT_LINK
      ? `http://${process.env.VM_INSERT_LINK}/insert/1/influx/write`
      : 'http://dev-user:PVwKrgvBoG5rcdAM2DoTBPdQk05ihzTCXwAY1ff1HlmsIBWS@vm-dev.linked-green.com/insert/1/influx/write';
  }

  static exportURL() {
    return process.env.VM_SELECT_LINK
      ? `http://${process.env.VM_SELECT_LINK}/select/1/prometheus/api/v1/export`
      : 'http://dev-user:PVwKrgvBoG5rcdAM2DoTBPdQk05ihzTCXwAY1ff1HlmsIBWS@vm-dev.linked-green.com/select/1/prometheus/api/v1/export';
  }

  static importURL() {
    return process.env.VM_INSERT_LINK
      ? `http://${process.env.VM_INSERT_LINK}/insert/1/prometheus/api/v1/import`
      : 'http://dev-user:PVwKrgvBoG5rcdAM2DoTBPdQk05ihzTCXwAY1ff1HlmsIBWS@vm-dev.linked-green.com/insert/1/prometheus/api/v1/import';
  }
}

export default VictoriaConfig;
