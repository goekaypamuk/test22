import { join } from 'path';
import { existsSync, mkdirSync } from 'fs';

const rootDir = process.cwd();

const filesDirectoryPath = join(rootDir, 'filesPath');
if (!existsSync(filesDirectoryPath)) {
  mkdirSync(filesDirectoryPath);
}
export default filesDirectoryPath;
