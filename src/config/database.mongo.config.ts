import { MongoClientOptions } from 'mongodb';
import { IDbConfig } from '../interface/database.config.interface';

class DbConfig {
  static getConfig(): IDbConfig {
    const user: string = process.env.MONGODB_USERNAME || 'mdb_user';
    const password: string = process.env.MONGODB_PASSWORD || 'DfSp93t7SOTCunObkir1O18dYdhZb9ARi3vcblXaUMmgXSOI';
    const link: string =
      process.env.MONGODB_LINK ||
      '116.202.45.30:27017,116.202.60.247:27017,88.198.114.54:27017/?replicaSet=lg1&ssl=false';
    return {
      user,
      password,
      link,
    };
  }

  static getConnectionString(): string {
    const conf = this.getConfig();
    return `mongodb://${conf.user}:${conf.password}@${conf.link}`;
  }

  static getConnectionOptions(): MongoClientOptions {
    return {};
  }
}

export default DbConfig;
