class Logger {
  origin: string;

  constructor(origin: string) {
    this.origin = origin;
  }

  log(msg: string) {
    console.log(`[${new Date().toISOString()}] [${this.origin}] ${msg}`);
  }

  err(msg: string) {
    console.error(`[${new Date().toISOString()}] [${this.origin}] ${msg}`);
  }
}

export default Logger;
