import * as cron from 'node-cron';
import { writeFile } from 'fs';
import moment from 'moment';
import { join } from 'path';
import filesDirectoryPath from './config/filespath.config';
import FileWatCher from './services/file.watcher.service';
import VictoriaMetric from './services/victoria.service';
import DatabaseMongo from './services/database.mongo.service';

class Main {
  private vmService: VictoriaMetric;

  private PACmetrics = `MS02_5M_W_PAC{_id_inverter="61b70f7b177dd12387f890b4"}`;

  private GPOAmetrics = `MS02V2_15M_Wm2_GPOA{_id_plant="61b36e77177dd12387e99f10"}`;

  private fsWatcher: FileWatCher;

  private databaseService: DatabaseMongo;

  constructor() {
    this.databaseService = new DatabaseMongo();
    this.vmService = new VictoriaMetric(this.databaseService);
    this.fsWatcher = new FileWatCher(this.vmService);
  }

  async init() {
    /**
                        ┌────────────── second (optional)
                        │ ┌──────────── minute
                        │ │ ┌────────── hour
                        │ │ │ ┌──────── day of month
                        │ │ │ │ ┌────── month
                        │ │ │ │ │ ┌──── day of week
                        │ │ │ │ │ │
                        │ │ │ │ │ │
                        * * * * * *
 */
    cron.schedule('*/5 * * * *', async () => {
      await this.callPACMetric();
    });
    cron.schedule('*/15 * * * *', async () => {
      await this.callGPOAMetric();
    });
  }

  async callPACMetric() {
    const start = Math.floor(moment().subtract(1, 'hour').valueOf() / 1000);
    const end = Math.floor(moment().valueOf() / 1000);
    const response = await this.vmService.export(this.PACmetrics, start, end);
    writeFile(join(filesDirectoryPath, `exported_${new Date().getTime()}.jsonl`), JSON.stringify(response), err => {
      if (err) {
        throw err;
      }
    });
  }

  async callGPOAMetric() {
    const start = Math.floor(moment().subtract(2, 'hour').valueOf() / 1000);
    const end = Math.floor(moment().valueOf() / 1000);
    const response = await this.vmService.export(this.GPOAmetrics, start, end);
    writeFile(join(filesDirectoryPath, `exported_${new Date().getTime()}.jsonl`), JSON.stringify(response), err => {
      if (err) {
        throw err;
      }
    });
  }
}

const main = new Main();
main.init();
