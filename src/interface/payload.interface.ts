export interface Metric {
  __name__: string;
  _id_inverter?: string;
  _id_asset?: string;
  _id_module_field?: string;
  _id_plant?: string;
  _id_portfolio?: string;
}

export interface VMPayloadI {
  metric?: Metric;
  values: number[];
  timestamps: number[];
}
