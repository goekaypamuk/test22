export interface IDbConfig {
  user: string;
  password: string;
  link: string;
}
