import {
  MongoClient,
  UpdateResult,
  Document,
  InsertManyResult,
  InsertOneResult,
  OptionalId,
  DeleteResult,
  ChangeStream,
} from 'mongodb';
import DbConfig from '../config/database.mongo.config';
import Deferred from '../class/deferred';
import Logger from '../class/logger';

class DatabaseMongo {
  connDef: Deferred<MongoClient>;

  private static instance: DatabaseMongo;

  private logger: Logger;

  public solarFactor = 0.888;

  private changeStream!: ChangeStream<Document>;

  constructor() {
    this.logger = new Logger('MongoDB');
    this.logger.log('Init MongoDb');
    this.connDef = new Deferred<MongoClient>();
    this.connect();
    this.initFactor();
  }

  async initFactor() {
    this.solarFactor = await this.getFactor();
    this.changeStream = await this.initWatch();
    this.changeStream.on('change', async next => {
      if (next.operationType === 'insert' || next.operationType === 'update') {
        console.log('received a change to the collection: \t');
        this.solarFactor = await this.getFactor();
      }
    });
  }

  static getInstance(): DatabaseMongo {
    if (DatabaseMongo.instance === undefined) {
      DatabaseMongo.instance = new DatabaseMongo();
    }
    return DatabaseMongo.instance;
  }

  async initWatch(): Promise<ChangeStream<Document>> {
    return this.watch('SEP', 'factor');
  }

  async connect(): Promise<MongoClient> {
    const client = await new MongoClient(DbConfig.getConnectionString()).connect();
    this.connDef.resolve(client);
    return this.connDef.promise;
  }

  async watch(database: string, collection: string): Promise<ChangeStream<Document>> {
    const con = await this.connDef.promise;
    const col = con.db(database).collection(collection);
    return col.watch();
  }

  async find(database: string, collection: string, filter: any = {}): Promise<any[]> {
    const con = await this.connDef.promise;
    const col = con.db(database).collection(collection);
    return col.find(filter).toArray();
  }

  async insertMany(database: string, collection: string, payload: any[] = []): Promise<InsertManyResult<Document>> {
    const con = await this.connDef.promise;
    const col = con.db(database).collection(collection);
    return col.insertMany(payload);
  }

  async insertOne(database: string, collection: string, doc: OptionalId<Document>): Promise<InsertOneResult<Document>> {
    const con = await this.connDef.promise;
    const col = con.db(database).collection(collection);
    return col.insertOne(doc);
  }

  async deleteOne(database: string, collection: string, filter: any = {}): Promise<DeleteResult> {
    const con = await this.connDef.promise;
    const col = con.db(database).collection(collection);
    return col.deleteOne(filter);
  }

  async replaceOne(
    database: string,
    collection: string,
    payload: any,
    filter: any = {}
  ): Promise<Document | UpdateResult> {
    const con = await this.connDef.promise;
    const col = con.db(database).collection(collection);
    return col.replaceOne(filter, payload, {
      upsert: true,
    });
  }

  async updateOne(
    database: string,
    collection: string,
    payload: any,
    filter: any = {}
  ): Promise<Document | UpdateResult> {
    const con = await this.connDef.promise;
    const col = con.db(database).collection(collection);
    return col.updateOne(
      filter,
      {
        $set: payload,
      },
      {
        upsert: true,
      }
    );
  }

  async getFactor(): Promise<number> {
    const def = new Deferred<number>();
    const currentTS = new Date().getTime();
    const con = await this.connDef.promise;
    const col = con.db('SEP').collection('factor');
    const factors = await col
      .find(
        {},
        {
          projection: { factor: 1, startingTS: 1 },
        }
      )
      .toArray();
    if (factors.length === 0) {
      def.resolve();
      return 0.163;
    }
    const sorted = factors.sort((a: any, b: any) => {
      const distancea = Math.abs(currentTS - a.startingTS);
      const distanceb = Math.abs(currentTS - b.startingTS);
      return distancea - distanceb;
    });
    def.resolve(sorted[0].factor);
    return def.promise;
  }
}

export default DatabaseMongo;
