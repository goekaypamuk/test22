/* eslint-disable no-restricted-syntax */
import axios from 'axios';
import readline from 'readline';
import { createReadStream } from 'fs';
import Logger from '../class/logger';
import VictoriaConfig from '../config/victoria-config';
import Deferred from '../class/deferred';
import DatabaseMongo from './database.mongo.service';
import { VMPayloadI } from '../interface/payload.interface';

const metricMapper: any = {
  MS02_5M_W_PAC: 'SEP_SL_ActPower_raw_mag_f',
  MS02V2_15M_Wm2_GPOA: 'SEP_SL_GlobRadiation_mag_f',
};
class VictoriaMetric {
  private logger: Logger;

  constructor(private database: DatabaseMongo) {
    this.logger = new Logger('Victoria');
    this.logger.log('Init');
  }

  import(data: any) {
    const def = new Deferred();
    if (data !== null) {
      axios
        .post(VictoriaConfig.importURL(), data, {
          headers: {
            maxContentLength: '2000000',
            maxBodyLength: '2000000',
            'Content-Type': 'application/json',
          },
        })
        .then(async (res: any) => {
          if (res.status === 204) {
            def.resolve(true);
          } else {
            console.log(`Import failed with status: ${res.status}`);
            this.logger.log(`Import failed with status: ${res.status}`);
          }
        })
        .catch((err: any) => {
          def.reject(`${err} (EXPORT)`);
        });
    } else {
      def.reject('NOt Parsable');
    }

    return def.promise;
  }

  async export(metric: string, start: number, end: number) {
    this.validDates(start, end);
    const def = new Deferred();
    axios
      .post(VictoriaConfig.exportURL(), null, {
        params: {
          'match[]': `${metric}`,
          start,
          end,
          max_rows_per_line: 40000,
        },
      })
      .then(async (res: any) => {
        if (res.status === 200) {
          def.resolve(res.data);
        } else {
          def.reject(`Export failed with status: ${res.status}`);
        }
      })
      .catch((err: any) => {
        def.reject(`${err} (EXPORT)`);
      });
    return def.promise;
  }

  private validDates(start: number, end: number) {
    const from = new Date(start).getTime();
    if (!(from > 0)) {
      throw new Error('Time Format Not Valid');
    }
    const to = new Date(end).getTime();
    if (!(to > 0)) {
      throw new Error('Time Format Not Valid');
    }
  }

  async importFromJson(filePath: string) {
    const def = new Deferred();
    try {
      const rl = readline.createInterface({
        input: createReadStream(filePath),
        terminal: false,
        crlfDelay: Infinity,
      });
      for await (const line of rl) {
        await this.import(this.parseRawData(line)).catch(e => console.log(e, 'ERROR', new Date()));
        const factorizedData = this.parseFactorData(line);
        if (factorizedData !== null) {
          await this.import(factorizedData).catch(e => console.log(e, 'ERROR', new Date()));
        }
      }
      def.resolve(true);
    } catch (err) {
      def.reject(err);
    }
  }

  parseRawData(data: string) {
    const d: VMPayloadI = this.isValidJson(data) ? JSON.parse(data) : null;
    if (d?.metric && d.metric.__name__ === 'MS02_5M_W_PAC') {
      delete d.metric._id_inverter;
      d.metric.__name__ = metricMapper[d.metric.__name__];
      d.metric._id_asset = 'xcAGv7vO5o0zPIck';
    } else if (d?.metric && d.metric.__name__ === 'MS02V2_15M_Wm2_GPOA') {
      d.metric.__name__ = metricMapper[d.metric.__name__];
      delete d.metric._id_module_field;
      delete d.metric._id_plant;
      delete d.metric._id_portfolio;
      delete d.metric._id_inverter;
      d.metric._id_asset = 'xcAGv7vO5o0zPIck';
    }
    return d;
  }

  parseFactorData(data: string) {
    const d: VMPayloadI = this.isValidJson(data) ? JSON.parse(data) : null;
    if (d?.metric?.__name__ === 'MS02V2_15M_Wm2_GPOA') {
      return null;
    }
    if (d?.metric) {
      delete d.metric._id_inverter;
      d.metric.__name__ = 'SEP_SL_ActPower_mag_f';
      d.metric._id_asset = 'xcAGv7vO5o0zPIck';
      d.values = d.values.map((t: any) => t * this.database.solarFactor);
    }

    return d;
  }

  private isValidJson(str: string) {
    try {
      if (str.length > 0) {
        JSON.parse(str);
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
    return true;
  }
}
export default VictoriaMetric;
