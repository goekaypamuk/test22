import chokidar from 'chokidar';
import { unlinkSync } from 'fs';
import filesDirectoryPath from '../config/filespath.config';
import Logger from '../class/logger';
import VictoriaMetric from './victoria.service';

const chokidarOptions: chokidar.WatchOptions = {
  persistent: true,
  followSymlinks: false,
  usePolling: true,
  depth: undefined,
  interval: 100,
  ignorePermissionErrors: false,
  awaitWriteFinish: true,
  ignored: /^\./,
};
class FileWatCher {
  private logger: Logger;

  private watcher: chokidar.FSWatcher;

  constructor(private vmService: VictoriaMetric) {
    this.logger = new Logger('File Watcher');
    this.watcher = chokidar.watch(filesDirectoryPath, chokidarOptions);
    this.init();
  }

  init() {
    this.watchDirectory();
    this.logger.log('Init File Watcher');
  }

  public watchDirectory() {
    this.watcher.on('add', async (path: any) => {
      await this.vmService.importFromJson(path).catch(e => {
        console.log(e);
        this.logger.log(e);
      });
      unlinkSync(path);
    });
  }
}

export default FileWatCher;
