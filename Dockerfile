FROM node:14-alpine as builder

WORKDIR /usr/src/app

COPY package.json .

RUN npm install

COPY . .

RUN npm run build:deploy

##################### Build end #######################

FROM node:14-alpine

WORKDIR /usr/src/app

COPY --chown=node:node --from=builder /usr/src/app/node_modules /usr/src/app/node_modules
COPY --chown=node:node  --from=builder /usr/src/app/dist /usr/src/app/dist
USER node

CMD ["node", "/usr/src/app/dist/main.js" ]