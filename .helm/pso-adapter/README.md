# Kubernetes Helm package for example service

## Installation

Install Helm package with default values:
```bash
helm install --namespace default --name example .
helm ls
```

Install Helm package to separated development environment:
```bash
helm install -f dev-custom-values.yaml --namespace development --name dev-example .
helm ls
```

## Upgrading

Upgrade already installed Helm package:
```bash
helm upgrade -f dev-custom-values.yaml dev-example .

```
